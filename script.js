/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?

Подія - це сигнал від браузру вашему коду, що щось на стровнки трапилось.
За допомогую подію відслідковують: переміщщення, міши, натискання клавиш на клавіатурі, скроли та інше.

2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
- click - коли миша клікає на елемент сторінки
- mousemove - переміщення мішки
- contextmenu - клік правою кнопкою міши на елементі сторинки
- mouseover/mouseout - коли курсор наводится або залишає елемент
- mousedown/mouseup - коли мішка натиснута або відпущена над елементом

3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

contextmenu - клік правою кнопкою міши на елементі сторинки. Ця подія використовується для відображення контекстного миню користувачу при натисканні правої кнопки миші.

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
  По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */

//Задача  1

const content = document.querySelector('#content')
const btn = document.querySelector('#btn-click')

btn.addEventListener('click', () => {
  let paragraf = document.createElement('p')
  paragraf.textContent = 'New Paragraph'
  content.prepend(paragraf)
})

// Задача 2

let btnсreate = document.createElement('button')
btnсreate.id = 'btn-input-create'
content.after(btnсreate)

let input = document.createElement('input')
input.type = 'text'
input.placeholder = 'Як тебе звати?'
input.name = 'name'
btnсreate.addEventListener('click', () => {
  btnсreate.after(input)
})
